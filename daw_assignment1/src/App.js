import React from 'react';
import './App.css';
import Navbar from "./layout/navbar";
import NewsPage from "./pages/News";
import AboutPage from "./pages/About";
import ContactPage from "./pages/Contact";
import CoordinatorPage from "./pages/Coordinator";
import StudentProfilePage from "./pages/StudentProfile";
import HomePage from "./pages/Home";
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Button from '@material-ui/core/Button';

function App() {
  return (
    <Router>
    <div>
      <Navbar/>
      <Switch>
      <Route path="/" exact component={HomePage}/>
      <Route path="/news" component={NewsPage}/>
      <Route path="/about" component={AboutPage}/>
      <Route path="/studentprofile" component={StudentProfilePage}/>
      <Route path="/coordinator" component={CoordinatorPage}/>
      <Route path="/contact" component={ContactPage}/>
      </Switch>
    </div>
    </Router>
  );
}
export default App;
