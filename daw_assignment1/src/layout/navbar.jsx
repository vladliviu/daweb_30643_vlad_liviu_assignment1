import React from "react";
import "./navbar.css";
import Button from "react-bootstrap/Button";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import FormControl from "react-bootstrap/FormControl";
import Form from "react-bootstrap/Form";
import { Link } from "react-router-dom";

export default function NavBar() {
  return (
    <div>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" fixed-top>
        <Navbar.Brand href="/">DawApp</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/news">Noutati</Nav.Link>
            <NavDropdown title="Profiles" id="collasible-nav-dropdown">
              <NavDropdown.Item href="/studentprofile">
                Student
              </NavDropdown.Item>
              <NavDropdown.Item href="/coordinator">
                Coordonator
              </NavDropdown.Item>
            </NavDropdown>
            <Nav.Link href="/about">Despre Lucrare</Nav.Link>
            <Nav.Link href="/contact">Contact</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
}
