import React, { Component } from "react";
import Navbar from "../layout/navbar";
import "./News.css";

export default function NewsPage() {
  return (
    <div class="container">
      <div class="card-deck row">
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="card">
            <div class="view overlay">
              <img
                class="card-img-top"
                src="http://news.mit.edu/sites/mit.edu.newsoffice/files/styles/news_article_image_top_slideshow/public/images/2020/MIT-Materials-Screening-01_0.jpg?itok=p3z39IBK"
                alt="Card image cap"
              />
              <a href="#!">
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
            <div class="card-body">
              <h4 class="card-title">Neural Networks</h4>
              <p class="card-text">
                Neural networks facilitate optimization in the search for new
                materials
              </p>
              <div>
                <a
                  href="http://news.mit.edu/2020/neural-networks-optimize-materials-search-0326"
                  class="btn btn-primary btn-sm"
                  target="_blank"
                >
                  Mai Mult
                </a>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="view overlay">
              <img
                class="card-img-top"
                src="http://news.mit.edu/sites/mit.edu.newsoffice/files/styles/news_article_image_top_slideshow/public/images/2020/MIT-Materials-Screening-01_0.jpg?itok=p3z39IBK"
                alt="Card image cap"
              />
              <a href="#!">
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
            <div class="card-body">
              <h4 class="card-title">Neural Networks</h4>
              <p class="card-text">
                Neural networks facilitate optimization in the search for new
                materials
              </p>
              <div>
                <a
                  href="http://news.mit.edu/2020/neural-networks-optimize-materials-search-0326"
                  class="btn btn-primary btn-sm"
                  target="_blank"
                >
                  Mai Mult
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="card">
            <div class="view overlay">
              <img
                class="card-img-top"
                src="http://news.mit.edu/sites/mit.edu.newsoffice/files/styles/news_article_image_top_slideshow/public/images/2020/MIT-Materials-Screening-01_0.jpg?itok=p3z39IBK"
                alt="Card image cap"
              />
              <a href="#!">
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
            <div class="card-body">
              <h4 class="card-title">Neural Networks</h4>
              <p class="card-text">
                Neural networks facilitate optimization in the search for new
                materials
              </p>
              <div>
                <a
                  href="http://news.mit.edu/2020/neural-networks-optimize-materials-search-0326"
                  class="btn btn-primary btn-sm"
                  target="_blank"
                >
                  Mai Mult
                </a>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="view overlay">
              <img
                class="card-img-top"
                src="http://news.mit.edu/sites/mit.edu.newsoffice/files/styles/news_article_image_top_slideshow/public/images/2020/MIT-Materials-Screening-01_0.jpg?itok=p3z39IBK"
                alt="Card image cap"
              />
              <a href="#!">
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
            <div class="card-body">
              <h4 class="card-title">Neural Networks</h4>
              <p class="card-text">
                Neural networks facilitate optimization in the search for new
                materials
              </p>
              <div>
                <a
                  href="http://news.mit.edu/2020/neural-networks-optimize-materials-search-0326"
                  class="btn btn-primary btn-sm"
                  target="_blank"
                >
                  Mai Mult
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
          <div class="card">
            <div class="view overlay">
              <img
                class="card-img-top"
                src="http://news.mit.edu/sites/mit.edu.newsoffice/files/styles/news_article_image_top_slideshow/public/images/2020/MIT-Materials-Screening-01_0.jpg?itok=p3z39IBK"
                alt="Card image cap"
              />
              <a href="#!">
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
            <div class="card-body">
              <h4 class="card-title">Neural Networks</h4>
              <p class="card-text">
                Neural networks facilitate optimization in the search for new
                materials
              </p>
              <div>
                <a
                  href="http://news.mit.edu/2020/neural-networks-optimize-materials-search-0326"
                  class="btn btn-primary btn-sm"
                  target="_blank"
                >
                  Mai Mult
                </a>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="view overlay">
              <img
                class="card-img-top"
                src="http://news.mit.edu/sites/mit.edu.newsoffice/files/styles/news_article_image_top_slideshow/public/images/2020/MIT-Materials-Screening-01_0.jpg?itok=p3z39IBK"
                alt="Card image cap"
              />
              <a href="#!">
                <div class="mask rgba-white-slight"></div>
              </a>
            </div>
            <div class="card-body">
              <h4 class="card-title">Neural Networks</h4>
              <p class="card-text">
                Neural networks facilitate optimization in the search for new
                materials
              </p>
              <div>
                <a
                  href="http://news.mit.edu/2020/neural-networks-optimize-materials-search-0326"
                  class="btn btn-primary btn-sm"
                  target="_blank"
                >
                  Mai Mult
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
