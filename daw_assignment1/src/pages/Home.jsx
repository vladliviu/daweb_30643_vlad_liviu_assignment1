import React, { Component } from "react";
import Navbar from "../layout/navbar";
import "./Home.css";

export default function HomePage() {
  return (
    <div>
      <section>
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6 order-lg-2">
              <div class="p-5">
                <img
                  class="img-fluid rounded-circle"
                  src="https://gomyprocurement.com/wp-content/uploads/2019/10/Header-manufacturing.jpg"
                  alt=""
                />
              </div>
            </div>
            <div class="col-lg-6 order-lg-1">
              <div class="p-5">
                <h2 class="display-4">Industria</h2>
                <p>
                  O industrie este o ramură a producției materiale și a
                  economiei naționale, care cuprinde totalitatea
                  întreprinderilor (uzine, centrale electrice, fabrici, mine
                  etc.) ocupate cu producția uneltelor de muncă, cu extracția
                  materiilor prime, a materialelor și combustibililor și cu
                  prelucrarea ulterioară a produselor obținute.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6">
              <div class="p-5">
                <img
                  class="img-fluid rounded-circle"
                  src="https://miro.medium.com/max/2400/1*c_fiB-YgbnMl6nntYGBMHQ.jpeg"
                  alt=""
                />
              </div>
            </div>
            <div class="col-lg-6">
              <div class="p-5">
                <h2 class="display-4">Machine Learning</h2>
                <p>
                  “ML este o disciplina stiintifica care exploreaza constructia
                  si studiul algoritmilor care invata din date Astfel de
                  algoritmi opereaza prin constructia unui model care se bazeaza
                  pe date de intrare si il foloseste pentru a face mai degraba
                  predictii sau decizii decat a urmari instructiuni programate
                  explicit"
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div class="container">
          <div class="row align-items-center">
            <div class="col-lg-6 order-lg-2">
              <div class="p-5">
                <img
                  class="img-fluid rounded-circle"
                  src="https://lh3.googleusercontent.com/proxy/uqdlSTP5y4YGsY-fX1siire-mtHlWuTGUC2OOCCaY8uvtHQRBcaekryw4OJs3skfdGCAyOOuIaxvY4gowhSSBkGpt96YhJmTun24J9c_1tVKkzCWrw"
                  alt=""
                />
              </div>
            </div>
            <div class="col-lg-6 order-lg-1">
              <div class="p-5">
                <h2 class="display-4">Ciclul de viata al produsului</h2>
                <p>
                  Ciclul de viață al produsului se referă la durata medie de
                  viață a unui produs: se face o analogie cu (produsele se nasc,
                  se dezvoltă, ajung la maturitate și apoi îmbătrânesc); în
                  funcție de perioada din viață în care se află produsul, sunt
                  influențate și vânzările acestuia.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
